module.exports = {
    botClientID: "1082589541304639530",
    botPrefix: "?",
    guildID: "1082394579225034752",
    ownerID: "896165554841337896",
    embedColor: "#00dbff",
    embedfooterText: "",

    ticketsSupportRoles: [
        "1082988861547872298",
        "1082988896918442094"
    ],
    ticketsHRSupportRoles: [
        "1082988896918442094"
    ],
    reportRoles: [
        "1082414560385966132",
		"1082506328116838480",
		"1082413169370222692",
		"1082413073182244865"
    ],
    ticketsOpenCategory: "1082991828686606396",
    ticketsCloseCategory: "1082991828686606396",
    ticketsTranscripts: "1082984516701913211",

    hrTicketsOpenCategory: "1083687852983984159",
    hrTicketsCloseCategory: "1083687852983984159",
    hrTicketsTranscripts: "1083687944629534790",
	
    reportOpenCategory: "1083702735792263168",
    reportCloseCategory: "1083702735792263168",
    reportTranscripts: "1083702634466250762"
}