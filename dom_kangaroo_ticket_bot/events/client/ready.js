module.exports = {
    name: 'ready',
    once: true,

    /**
     * @param {Client} client 
     */
    async execute(client) {
        
        // Puts an activity
        client.user.setActivity("Kangaroo Tickets#4798", {
            type: "WATCHING",
            name: "Jake's Kangaroos"
        });
        
        // Send a message on the console
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();
        console.log(datetime)
            console.log(`✔ ${client.user.tag} | ${client.user.id} is online.`);
            console.log(`Online at ${datetime}`);
            guild.channels.cache.get('1083241069824704653').send(`<@1082589541304639530> is now online\n${datetime}`);
    }
}
