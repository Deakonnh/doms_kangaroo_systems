const { Client, ActivityType, EmbedBuilder, Guild } = require('discord.js');

module.exports = (client, guild) => {
        client.user.setActivity({
        name: "Jake's Kangaroos",
        type: ActivityType.Watching,
    })


client.on('guildMemberAdd', guildMember =>{
    let welcomeRole = guildMember.guild.roles.cache.find(role => role.name === '• Non-Verified •');

    guildMember.roles.add(welcomeRole);

    const welcomeEmbed = new EmbedBuilder()
    .setColor('Green')
    .setTitle(`Jake's Kangaroos | Welcome`)
    .setDescription(`Welcome to Jake's Kangaroos <@${guildMember.user.id}>! We are glad to have you hop into our community today. Please take the time to read over our <#1082396357148561499> and <#1082416850488541256> Channels before submitting an application to join our community.`)

guild.channels.cache.get('1082411042895839303').send({embeds: [welcomeEmbed]});
});
}