const { Client, Guild, User, EmbedBuilder } = require('discord.js')

module.exports = (client, guild) => {

client.on("messageUpdate", function(oldMessage, newMessage, time){
    if (oldMessage.author.bot) {
        return;
    } 
    const chatlogembed = new EmbedBuilder()
    .setColor('Purple')
    .setTitle('Message Edited')
    .setAuthor({name: `${newMessage.author.tag} | ${newMessage.author.id}`, iconURL: `${newMessage.author.avatarURL()}`})
    .setDescription(`Message in Channel <#${newMessage.channel.id}> | Edited at <t:${parseInt(newMessage.createdTimestamp / 1000)}:F>`)
    .addFields(
        { name: 'Original Message', value: `${oldMessage}`, inline: false},
        { name: 'Edited Message', value: `${newMessage}`, inline: false},
    )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })

guild.channels.cache.get('1082528152028463154').send({embeds: [chatlogembed]});
})
};
