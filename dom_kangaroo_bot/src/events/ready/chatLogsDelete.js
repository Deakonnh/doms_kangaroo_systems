const { Client, Guild, User, EmbedBuilder } = require('discord.js')

module.exports = (client, guild) => {

client.on('messageDelete', function(message, channel){
    if (message.author.bot) {
        return;
    } 
    const chatlogdeleteembed = new EmbedBuilder()
    .setColor('Blue')
    .setTitle('Message Deleted')
    .setAuthor({name: `${message.author.tag} | ${message.author.id}`, iconURL: `${message.author.avatarURL()}`})
    .setDescription(`Message in Channel <#${message.channel.id}> | Deleted at <t:${parseInt(message.createdTimestamp / 1000)}:F>`)
    .addFields(
        { name: 'Message', value: `${message}`, inline: false},
    )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })

guild.channels.cache.get('1082528152028463154').send({embeds: [chatlogdeleteembed]});
})
};
