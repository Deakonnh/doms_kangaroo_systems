const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
  Guild,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
callback: async (client, interaction, guild) => {
const administratorId = interaction.user.id;
const targetUserId = interaction.options.get('target-user').value;
const role =
      interaction.options.get('role').value;

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    const rolePosition = role.position;
    const targetUserRolePosition = targetUser.roles.highest.position; // Highest role of the target user
    const requestUserRolePosition = interaction.member.roles.position; // Highest role of the user running the cmd
    const botRolePosition = interaction.guild.members.me.roles.highest.position; // Highest role of the bot

    console.log(interaction.user.roles.position)
    console.log(role.position)
    console.log(requestUserRolePosition)
    console.log(targetUserRolePosition)

    if (requestUserRolePosition <= role.position) {
      await interaction.editReply(
        "You can't assign that role as it is above or equal to your highest role"
      );
      return;
    }
    if (rolePosition >= botRolePosition)
      return interaction.editReply(
        "I can't assign that role as it is above or equal to my highest role"
      );

    // Assign the targetUser role
    try {
      await targetUser.roles.add(role);
      await interaction.editReply(
        `A role was given to the user ${targetUser}\nRole: <@&${role}>`
      );
    } catch (error) {
      console.log(`There was an error when adding a role: ${error}`);
    }
    const channel = client.channels.cache.get('1082528122181783592'); 
    const addRoleLogEmbed = new EmbedBuilder()
    .setColor('Yellow')
    .setTitle(`Role Added`)
    .addFields(
      { name:`Administrator`, value:`<@${administratorId}>`, inline:true },       
      { name:`User`, value:`${targetUser}`, inline:true }, 
      { name:`Role`, value:`<@&${role}>`, inline:true },
    )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    channel.send({embeds: [addRoleLogEmbed]});
  },

//const role = interaction.options.getRole();
//const member = interaction.options.getMember('target');
//member.roles.add(role);

  name: 'addrole',
  description: 'Adds a role to a user',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to add a role to.',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'role',
      description: 'The role you want to add to a user.',
      type: ApplicationCommandOptionType.Role,
      required: true,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.ManageRoles],
//  botPermissions: [PermissionFlagsBits.ManageRoles],

};