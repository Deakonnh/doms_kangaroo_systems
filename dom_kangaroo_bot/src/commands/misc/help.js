const { Client, IntentsBitField, EmbedBuilder, ActivityType } = require('discord.js');

module.exports = {
  name: 'help',
  description: 'Provides a list of commands',

  callback: async (client, interaction) => {
//    await interaction.deferReply();

    const embedhelp = new EmbedBuilder()
        .setTitle('KangarooBot Commands')
        .setDescription('Commands Performed by the KangarooBot')
        .setColor('Purple')
        .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://i.imgur.com/LXhGwoo.gif' })
        .addFields({
            name: '/hey',
            value: 'When run the bot will reply with Hey!',
            inline: true,
         },
         {
            name: '/ping',
            value: 'When run the bot will reply with its ping!',
            inline: true,
         },
         {
            name: '/add',
            value: 'Adds two unique numbers together',
            inline: true,
         },
         {
            name: '/embed',
            value: 'Allows users to create an embed',
            inline: true,
         },
         {
            name: '/addrole',
            value: 'Allows user to add a role to another user',
            inline: true,
         },
         {
            name: '/removerole',
            value: 'Allows user to remove a role to another user',
            inline: true,
         },
         {
            name: '/temprole',
            value: 'Temporarily gives a role to a user',
            inline: true,
         },
         {
            name: '/kick',
            value: 'Kicks a user from the server',
            inline: true,
         },
         {
            name: '/ban',
            value: 'Bans a user from the server',
            inline: true,
         },
         {
            name: '/mute',
            value: 'Mutes a member for a given duration',
            inline: true,
         },
         {
            name: '/help',
            value: 'Provides a list of avaliable commands',
            inline: true,
         });


     interaction.reply({ embeds: [embedhelp] }); 
  }
};
