const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
callback: async (client, interaction) => {
const administratorId = interaction.user.id;
const targetUserId = interaction.options.get('target-user').value;
const role =
      interaction.options.get('role').value;

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }



    // Assign the targetUser role
    try {
      await targetUser.roles.remove(role);
      await interaction.editReply(
        `User ${targetUser} had a role removed from them\nRole: <@&${role}>`
      );
    } catch (error) {
      console.log(`There was an error when removing a role: ${error}`);
    }

    const channel = client.channels.cache.get('1082528122181783592'); 
    const removeRoleLogEmbed = new EmbedBuilder()
    .setColor('Red')
    .setTitle(`Role Removed`)
    .addFields(
      { name:`Administrator`, value:`<@${administratorId}>`, inline:true },       
      { name:`User`, value:`${targetUser}`, inline:true }, 
      { name:`Role`, value:`<@&${role}>`, inline:true },
    )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    channel.send({embeds: [removeRoleLogEmbed]});
  },

//const role = interaction.options.getRole();
//const member = interaction.options.getMember('target');
//member.roles.add(role);

  name: 'removerole',
  description: 'Removes a role from a user',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to remove a role from.',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'role',
      description: 'The role you want to remove from a user.',
      type: ApplicationCommandOptionType.Role,
      required: true,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.ManageRoles],
//  botPermissions: [PermissionFlagsBits.ManageRoles],

};