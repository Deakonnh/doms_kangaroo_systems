const { Client, IntentsBitField, EmbedBuilder, ActivityType, ApplicationCommandOptionType } = require('discord.js');

module.exports = {
  name: 'embed',
  description: 'Allows Custom Embeds to be Generated',
  options: [
    {
      name: 'title',
      description: 'Title of the Embed',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
    {
      name: 'description',
      description: 'Description of the Embed',
      type: ApplicationCommandOptionType.String,
    },
    {
      name: 'field-title',
      description: 'Title of the first field',
      type: ApplicationCommandOptionType.String,
    },
    {
      name: 'field-value',
      description: 'Description of the first field',
      type: ApplicationCommandOptionType.String,
    },
  ],

  callback: async (client, interaction) => {

     const embed = new EmbedBuilder()
        .setTitle(interaction.options.get('title').value)
        .setDescription(interaction.options.get('description').value)
        .setColor('Purple')
        .addFields({
            name: interaction.options.get('field-title').value,
            value: interaction.options.get('field-value').value,
            inline: true,
         },
         )
        .setFooter({ text: 'This embed was generated using a bot designed by Dom_#7183', iconURL: 'https://i.imgur.com/LXhGwoo.gif' })
        .setTimestamp();

     interaction.reply({ embeds: [embed] }); 
}
};