const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
  Guild,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
callback: async (client, interaction, guild) => {
const administratorId = interaction.user.id;
const targetUserId = interaction.options.get('target-user').value;
const duration = interaction.options.get('duration').value;
const role =
      interaction.options.get('role').value;

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    // Assign the targetUser role
    try {
      await targetUser.roles.add(role);

    setTimeout(() => {
      targetUser.roles.remove(role);
    }, duration * 1000);

      await interaction.editReply(
        `A role was temporarily given to the user ${targetUser}\nRole: <@&${role}>`
      );
    } catch (error) {
      console.log(`There was an error when adding a role: ${error}`);
    }
//  const channel = client.channels.cache.get('1081822651842043965'); 
    const tempRoleLogEmbed = new EmbedBuilder()
    .setColor('Green')
    .setTitle(`Temprole Assigned`)
    .addFields(
      { name:`Administrator`, value:`<@${administratorId}>`, inline:true },       
      { name:`User`, value:`${targetUser}`, inline:true }, 
      { name:`Role`, value:`<@&${role}>`, inline:true },
      { name:`Duration`, value:`${duration} seconds`, inline:true }, 
    )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    client.channels.cache.get('1082528122181783592').send({embeds: [tempRoleLogEmbed]});
  },



  name: 'temprole',
  description: 'Temporarily gives a user a role',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to add a role to.',
      type: ApplicationCommandOptionType.User,
      required: true,
    },
    {
      name: 'role',
      description: 'The role you want to add to a user.',
      type: ApplicationCommandOptionType.Role,
      required: true,
    },
    {
      name: 'duration',
      description: 'How long the user should have the role for.',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.ManageRoles],
//  botPermissions: [PermissionFlagsBits.ManageRoles],

};