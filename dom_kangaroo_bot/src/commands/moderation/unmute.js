const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
  Guild,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
callback: async (client, interaction, guild) => {
const logchannel = client.channels.cache.get('1082528182839808020')
const channel = interaction.channel;
const administratorID = interaction.user.id;
const targetUserId = interaction.options.get('target-user').value;
const role = interaction.guild.roles.cache.find(role => role.name === 'JK | KangarooBot Muted');

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    // Assign the targetUser role
    try {
      await targetUser.roles.remove(role);

      await interaction.editReply(
        `Done`
      );
    } catch (error) {
      console.log(`There was an error when unmuting: ${error}`);
    }    

    const unmutedEmbed = new EmbedBuilder()
    .setColor('Purple')
    .setTitle(`Member Unmuted`)
//    .setAuthor({name: `${targetUser}`, iconURL: `https://img.myloview.com/stickers/default-avatar-profile-in-trendy-style-for-social-media-user-icon-400-228654852.jpg`})
//    .setDescription(`Muted by <@${administratorID}> | Muted for ${duration}`)
    .addFields(
        { name: 'User Unmuted', value: `${targetUser}`, inline: true},
        { name: 'UnMuted by', value: `<@${administratorID}>`, inline: true},
      )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    channel.send({embeds: [unmuteEmbed]});
    logchannel.send({embeds: [unmutedEmbed]});
  },



  name: 'unmute',
  description: 'Unmutes a user.',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to unmute.',
      type: ApplicationCommandOptionType.Mentionable,
      required: true,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.ManageRoles],
//  botPermissions: [PermissionFlagsBits.ManageRoles],

};