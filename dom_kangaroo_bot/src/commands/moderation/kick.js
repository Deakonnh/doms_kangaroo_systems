const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */

  callback: async (client, interaction) => {
    const administratorID = interaction.user.id;
    const timeStamp = interaction.createdTimestamp
    const targetUserId = interaction.options.get('target-user').value;
    const reason =
      interaction.options.get('reason')?.value || 'No reason provided';

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    if (targetUser.id === interaction.guild.ownerId) {
      await interaction.editReply(
        "You can't kick that user because they're the server owner."
      );
      return;
    }

    const targetUserRolePosition = targetUser.roles.highest.position; // Highest role of the target user
    const requestUserRolePosition = interaction.member.roles.highest.position; // Highest role of the user running the cmd
    const botRolePosition = interaction.guild.members.me.roles.highest.position; // Highest role of the bot

    if (targetUserRolePosition >= requestUserRolePosition) {
      await interaction.editReply(
        "You can't kick that user because they have the same/higher role than you."
      );
      return;
    }

    if (targetUserRolePosition >= botRolePosition) {
      await interaction.editReply(
        "I can't kick that user because they have the same/higher role than me."
      );
      return;
    }

    // Kick the targetUser
    try {
      await targetUser.kick({ reason });
      await interaction.editReply(
        `User ${targetUser} was kicked\nReason: ${reason}`
      );
    } catch (error) {
      console.log(`There was an error when Kicking: ${error}`);
    }

    const kickLogEmbed = new EmbedBuilder()
    .setColor('Yellow')
    .setTitle(`Member Kicked`)
    .setAuthor({name: `${targetUser}`})
    .setDescription(`Kicked by <@${administratorID}> | Kicked at <t:${parseInt(timeStamp / 1000)}:F>`)
    .addFields(
        { name: 'User Kicked', value: `${targetUser}`, inline: false},
        { name: 'Reason', value: `${reason}`, inline: false},
      )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    client.channels.cache.get('1082528182839808020').send({embeds: [kickLogEmbed]});
  },

  name: 'kick',
  description: 'Kicks a member from this server.',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to kick.',
      type: ApplicationCommandOptionType.Mentionable,
      required: true,
    },
    {
      name: 'reason',
      description: 'The reason you want to kick a member.',
      type: ApplicationCommandOptionType.String,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.KickMembers],
//  botPermissions: [PermissionFlagsBits.KickMembers],
};