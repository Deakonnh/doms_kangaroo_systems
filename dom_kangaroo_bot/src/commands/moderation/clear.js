const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */

  callback: async (client, interaction) => {
    const administratorID = interaction.user.id;
    const timeStamp = interaction.createdTimestamp;
    const channel = interaction.channel.id;
    const numInput = interaction.options.get('value').value;
    const amount = numInput + 1
//   let myFunc = num => Number(num);
//    const amount = Array.from(String(number), myFunc);

    await interaction.deferReply();

if (!amount) return await interaction.editReply({content: "Please specify the amount of messages you want to delete", ephemeral: true});
if (amount > 100 || amount < 1 ) return await interaction.editReply({content: "Please select a number between 1-100", ephemeral: false});


    try {
      await interaction.channel.bulkDelete(amount);
//      await interaction.editReply(
//        `Cleared ${amount} of messages from <#${channel}> :broom:`
//      );
      setTimeout(function(){ 
        interaction.channel.send(`Cleared ${amount - 1} messages :broom:`); 
      }, 100)
    } catch (error) {
      console.log(`There was an error when Clearing: ${error}`);
    }

    const clearLogEmbed = new EmbedBuilder()
    .setColor('Yellow')
    .setTitle(`Messages Cleared`)
    .setDescription(`Messages cleared by <@${administratorID}> | Cleared at <t:${parseInt(timeStamp / 1000)}:F>`)
    .addFields(
        { name: 'Messages Cleared', value: `${amount}`, inline: false},
        { name: 'Cleared in Channel', value: `<#${channel}>`, inline: false},
      )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    client.channels.cache.get('1082528182839808020').send({embeds: [clearLogEmbed]});
  },

    name: 'clear',
    description: 'Clear messages!',
    options: [
    {
      name: 'value',
      description: 'Amount of message to clear.',
      type: ApplicationCommandOptionType.Integer,
      required: true,
    },
    ]
};
