const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */

  callback: async (client, interaction) => {
    const administratorID = interaction.user.id;
    const timeStamp = interaction.createdTimestamp;
    const targetUserId = interaction.options.get('target-user').value;
    const reason =
      interaction.options.get('reason')?.value || 'No reason provided';

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    if (targetUser.id === interaction.guild.ownerId) {
      await interaction.editReply(
        "You can't ban that user because they're the server owner."
      );
      return;
    }

    const targetUserRolePosition = targetUser.roles.highest.position; // Highest role of the target user
    const requestUserRolePosition = interaction.member.roles.highest.position; // Highest role of the user running the cmd
    const botRolePosition = interaction.guild.members.me.roles.highest.position; // Highest role of the bot

    if (targetUserRolePosition >= requestUserRolePosition) {
      await interaction.editReply(
        "You can't ban that user because they have the same/higher role than you."
      );
      return;
    }

    if (targetUserRolePosition >= botRolePosition) {
      await interaction.editReply(
        "I can't ban that user because they have the same/higher role than me."
      );
      return;
    }

    // Ban the targetUser
    try {
      await targetUser.ban({ reason });
      await interaction.editReply(
        `User ${targetUser} was banned\nReason: ${reason}`
      );
    } catch (error) {
      console.log(`There was an error when banning: ${error}`);
    }
    
    const banLogEmbed = new EmbedBuilder()
    .setColor('Red')
    .setTitle(`Member Banned`)
    .setAuthor({name: `${targetUser}`, iconURL: `https://img.myloview.com/stickers/default-avatar-profile-in-trendy-style-for-social-media-user-icon-400-228654852.jpg`})
    .setDescription(`Banned by <@${administratorID}> | Banned at <t:${parseInt(timeStamp / 1000)}:F>`)
    .addFields(
        { name: 'User Banned', value: `${targetUser}`, inline: false},
        { name: 'Reason', value: `${reason}`, inline: false},
      )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    client.channels.cache.get('1082528182839808020').send({embeds: [banLogEmbed]});
},



  name: 'ban',
  description: 'Bans a member from this server.',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to ban.',
      type: ApplicationCommandOptionType.Mentionable,
      required: true,
    },
    {
      name: 'reason',
      description: 'The reason you want to ban.',
      type: ApplicationCommandOptionType.String,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.BanMembers],
//  botPermissions: [PermissionFlagsBits.BanMembers],
  
    
};