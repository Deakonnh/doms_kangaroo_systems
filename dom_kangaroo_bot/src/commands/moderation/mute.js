const {
  Client,
  Interaction,
  ApplicationCommandOptionType,
  PermissionFlagsBits,
  EmbedBuilder,
  Guild,
} = require('discord.js');

module.exports = {
  /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
callback: async (client, interaction, guild) => {
const logchannel = client.channels.cache.get('1082528182839808020')
const channel = interaction.channel;
const administratorID = interaction.user.id;
const targetUserId = interaction.options.get('target-user').value;
const duration = interaction.options.get('duration').value;
const role = interaction.guild.roles.cache.find(role => role.name === 'JK | KangarooBot Muted');

    await interaction.deferReply();

    const targetUser = await interaction.guild.members.fetch(targetUserId);

    if (!targetUser) {
      await interaction.editReply("That user doesn't exist in this server.");
      return;
    }

    // Assign the targetUser role
    try {
      await targetUser.roles.add(role);

    setTimeout(() => {
      targetUser.roles.remove(role);
    }, duration * 1000);

      await interaction.editReply(
        `Done`
      );
    } catch (error) {
      console.log(`There was an error when muting: ${error}`);
    }    

    const mutedEmbed = new EmbedBuilder()
    .setColor('Purple')
    .setTitle(`Member Muted`)
//    .setAuthor({name: `${targetUser}`, iconURL: `https://img.myloview.com/stickers/default-avatar-profile-in-trendy-style-for-social-media-user-icon-400-228654852.jpg`})
//    .setDescription(`Muted by <@${administratorID}> | Muted for ${duration}`)
    .addFields(
        { name: 'User Muted', value: `${targetUser}`, inline: true},
        { name: 'Muted by', value: `<@${administratorID}>`, inline: true},
        { name: 'Duration', value: `${duration}`, inline: true}
      )
    .setFooter({ text: 'Developed by Dom_#7183', iconURL: 'https://wallpapers.com/images/hd/cool-profile-pictures-panda-car-pswltzd2oewsl4wt.jpg' })
    channel.send({embeds: [mutedEmbed]});
    logchannel.send({embeds: [mutedEmbed]});
  },



  name: 'mute',
  description: 'Mutes a user.',
  options: [
    {
      name: 'target-user',
      description: 'The user you want to mute.',
      type: ApplicationCommandOptionType.Mentionable,
      required: true,
    },
    {
      name: 'duration',
      description: 'How long the user should be muted. In Seconds.',
      type: ApplicationCommandOptionType.String,
      required: true,
    },
  ],
//  permissionsRequired: [PermissionFlagsBits.ManageRoles],
//  botPermissions: [PermissionFlagsBits.ManageRoles],

};