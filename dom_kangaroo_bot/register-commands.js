require('dotenv').config();
const { REST, Routes, ApplicationCommandOptionType } = require('discord.js');

const commands = [
    {
      name: 'addrole',
      description: 'Adds a role to a user',
      options: [
          {
          name: 'user',
          description: 'User to assign role',
          type: ApplicationCommandOptionType.User,
          required: true,
          },
          {
          name: 'role',
          description: 'Role to assign user',
          type: ApplicationCommandOptionType.Role,
          required: true,
          }
    ]
    },  
    {
      name: 'removerole',
      description: 'Removes a role from a user',
      options: [
          {
          name: 'user',
          description: 'User to unassign role from',
          type: ApplicationCommandOptionType.User,
          required: true,
          },
          {
          name: 'role',
          description: 'Role to unassign from user',
          type: ApplicationCommandOptionType.Role,
          required: true,
          }
    ]
    },  
    {
        name: 'help',
        description: 'Provides a list of avaliable commands',
    },   
    {
        name: 'embed',
        description: 'Sends an embed!',
        options: [
            {
            name: 'title',
            description: 'Embed Title',
            type: ApplicationCommandOptionType.String,
            required: true,
            },
            {
            name: 'description',
            description: 'Embed Description',
            type: ApplicationCommandOptionType.String,
            required: true,
            },
            {
            name: 'field-title',
            description: '1st Field Title',
            type: ApplicationCommandOptionType.String,
            required: true,
            },
            {
            name: 'field-value',
            description: '1st Field Value',
            type: ApplicationCommandOptionType.String,
            required: true,
            },
    ]
    },
    {
        name: 'add',
        description: 'Adds two numbers.',
        options: [
           {
            name: 'first-number',
            description: 'The first number.',
            type: ApplicationCommandOptionType.Number,
            required: true,
            },
            {
            name: 'second-number',
            description: 'The second number.',
            type: ApplicationCommandOptionType.Number,
            required: true,
            }
        ]
    },
  {
    name: 'hey',
    description: 'Replies with hey!',
  },
  {
    name: 'ping',
    description: 'Replies with Pong!',
  },
];

const rest = new REST({ version: '10' }).setToken(process.env.TOKEN);

(async () => {
  try {
    console.log('Registering slash commands...');

    await rest.put(
      Routes.applicationGuildCommands(
        process.env.CLIENT_ID,
        process.env.GUILD_ID
      ),
      { body: commands }
    );

    console.log('Slash commands were registered successfully!');
  } catch (error) {
    console.log(`There was an error: ${error}`);
  }
})();