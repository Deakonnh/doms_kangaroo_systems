require('dotenv').config();
const {
  Client,
  IntentsBitField,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
} = require('discord.js');

const client = new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ],
});

const roles = [
  {
    id: '1082524173282312302',
    label: 'CaliRP | Staff Team',
  },
  {
    id: '1082580041474527253',
    label: 'CaliRP | Verified Civilian',
  },
  {
    id: '1082524104210518046',
    label: 'CaliRP | Law Enforcement',
  },
  {
    id: '1082524739031011338',
    label: 'CaliRP | DHS',
  },
  {
    id: '1082524804265037986',
    label: 'CaliRP | CIA',
  },
  {
    id: '1082524169108979712',
    label: 'CaliRP | Content Creator',
  },
  {
    id: '1082524164054859816',
    label: 'CaliRP | Department of Defense',
  },  
  {
    id: '1083334047956222002',
    label: 'JK | Event Notified'
  },
];

client.on('ready', async (c) => {
  try {
    const channel = await client.channels.cache.get('1082525527497248840');
    if (!channel) return;

    const row = new ActionRowBuilder();

    roles.forEach((role) => {
      row.components.push(
        new ButtonBuilder()
          .setCustomId(role.id)
          .setLabel(role.label)
          .setStyle(ButtonStyle.Primary)
      );
    });

    await channel.send({
      content: 'Add or remove roles below!',
      components: [row],
    });
    process.exit();
  } catch (error) {
    console.log(error);
  }
});

client.login(process.env.TOKEN);